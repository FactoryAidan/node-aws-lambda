import Lambda from '../src/Lambda/index.js';
// Lambda.deploy();

import {handler} from './handler.js';
import event from './example.event.json';
import context from './example.context.json';

// handler(event,context);

Lambda.execute(handler,event,context);