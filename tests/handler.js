import https from 'https';

export const handler = async (event,context) => {

	console.log('🚦 Test Starting');

	const result = await new Promise(function(resolve,reject){
		https.get('https://www.google.com/', (resp) => {

			console.log("\t",'... HTTP Status:',resp.statusCode);
		
			let data = '';

			// A chunk of data has been received.
			resp.on('data', (chunk) => {
				data += chunk;
			});

			// The whole response has been received. Print out the result.
			resp.on('end', () => {
				console.log("\t",'... body:',data.slice(0,50));
				resolve(data);
			});

		}).on("error", (err) => {
			console.log("Error: " + err.message);
			reject(err.message);
		});
	});
	
	console.log('🛑 Test Stopping');
	
	const response = {
		statusCode: 200,
		body: JSON.stringify('Hello from Lambda!'),
	};
	return response;

};//handler
