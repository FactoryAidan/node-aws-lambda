import yargs from 'yargs';
import {hideBin} from 'yargs/helpers';
const argv	=	yargs( hideBin(process.argv) ).argv;

import Setup from '../Setup/index.js';
import Lambda from '../Lambda/index.js';

const acceptableDoArgumentValues = [
	'deploy',
	'deployCode',
	'deployConfiguration',
	'execute',
	'setupLocal',
];

switch( argv.do ){
	case 'deploy':
		console.log('🎬 Cloud ƛFunction update started...');
		Lambda.deploy().then(response=>{
			console.log('🏁 Cloud ƛFunction update complete.');
		});
		break;
	case 'deployCode':
		console.log('🎬 Cloud ƛFunction update started...');
		Lambda.deployCode().then(response=>{
			console.log('🏁 Cloud ƛFunction update complete.');
		});
		break;
	case 'deployConfiguration':
		console.log('🎬 Cloud ƛFunction update started...');
		Lambda.deployConfiguration().then(response=>{
			console.log('🏁 Cloud ƛFunction update complete.');
		});
		break;
	case 'execute':
		console.log('🎬 Local ƛFunction execution started...');
		Lambda.execute(argv.testEventFileLocation).then(response=>{
			console.log('🏁 Local ƛFunction execution complete.');
		});
		break;
	case 'setupLocal':
		console.log('⚙️ Local Environment setup started...');
		Setup.local().then(response=>{
			console.log('🏁 Local Environment setup complete.');
		});
		break;
	default:
		console.log(`🚨 Whoops! Acceptable '--do' values are: ${acceptableDoArgumentValues.join('|')}`);
}//switch
