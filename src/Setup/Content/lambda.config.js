//	Reference:
//	https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-lambda/interfaces/updatefunctionconfigurationcommandinput.html

const UpdateFunctionConfigurationCommandInput = {
	// DeadLetterConfig
	// Description
	Environment:    {
		// Variables:  {},	//	DO NOT ASSIGN VARIABLES HERE! Use `.env.lambda` instead. Populate it just like you would a normal `.env`.
	},
	// FileSystemConfigs
	FunctionName		:	'test_internet_subnet',
	Handler				:	'index.handler',		//	{Filename}.{Export} that will handle
	// ImageConfig
	// KMSKeyArn
	// Layers
	// MemorySize
	// RevisionId
	// Role
	Runtime				:	'nodejs14.x',
	// Timeout
	// TracingConfig
	// VpcConfig
};

export default UpdateFunctionConfigurationCommandInput;