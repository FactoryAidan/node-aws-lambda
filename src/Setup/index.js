import FS from 'fs/promises';
import {fileURLToPath} from 'url';
import path from 'path';

class Setup {

	static from(){
		const instance = new Setup();
		return instance;
	}

	async local(){
		const promises = [];

		promises.push(this._writeFile('.env'));
		promises.push(this._writeFile('.env.lambda'));
		promises.push(this._writeFile('index.js'));
		promises.push(this._writeFile('lambda.config.js'));
		promises.push(this._writeFile('test.context.json'));
		promises.push(this._writeFile('test.event.json'));

		return Promise.all(promises);
	}

	async _writeFile(filename){

		const __filename = fileURLToPath(import.meta.url);
		const __dirname = path.dirname(__filename);
		// console.log('directory-name 👉️', __dirname);

		return FS.access(filename).then(
			function(){
				console.log("\t",`🙅 File Exists Already. Will not overwrite: ${filename}`);
			},
			function(error){
				return FS.readFile(`${__dirname}/Content/${filename}`).then(
					function(data){
						return FS.writeFile(filename,data).then(function(){
							console.log("\t",`✅ Created local file: ${filename}`);
						});
					},
					function(error){
						console.error(error);
					}
				);
			}
		);
	}

};

export default Setup.from();