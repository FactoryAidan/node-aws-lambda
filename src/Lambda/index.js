import {
	LambdaClient,
	UpdateFunctionCodeCommand,
	UpdateFunctionConfigurationCommand,
	// GetFunctionCommand,
	waitUntilFunctionUpdated,
} from '@aws-sdk/client-lambda';
import FS from 'fs/promises';
import Paths from '../Helpers/Paths/index.js';
import Zip from '../Helpers/Zip/index.js';
import failsafeImport from '../Helpers/Import/index.js'

import dotenv from 'dotenv'
dotenv.config({ path: `${process.env.INIT_CWD}/.env` });
const {parsed: dot_env_dot_lambda} = dotenv.config({ path: `${process.env.INIT_CWD}/.env.lambda` });

const {default: UpdateFunctionConfigurationCommandInput} = await failsafeImport(`${process.env.INIT_CWD}/lambda.config.js`);
// console.log(UpdateFunctionConfigurationCommandInput);

class Lambda {

	defaults	=	{
		//	These are Regular Expression patterns that we feed directly into `new RegExp()`
		//	So they must be escaped.
		ignore_patterns		:	[
			'\\.DS_Store',
			'\\.env',
			'\\.git',
			'@factoryaidan',
			'lambda\\.config\\.js',
			'package\\.lock',
			'test\\.',
		],
		source_directory			:	Paths.real_cwd.relative,
	};

	constructor(){
		const ignore_patterns	=	this.defaults.ignore_patterns.concat(process.env.AWS_LAMBDA_IGNORE_SOURCE_PATTERNS?.split(','));
		this.ignore_patterns	=	ignore_patterns.filter(_=>_);
		this.source_directory	=	process.env.AWS_LAMBDA_SOURCE_DIR || './';
		this.config				=	UpdateFunctionConfigurationCommandInput;
		this.client				=	this._getClient();

	}

	static from(){
		const instance = new Lambda();
		return instance;
	}

	_getClient(){

		const LambdaClientConfig = {
			// credentials			:	{},
			customUserAgent		:	'node-aws-lambda',
			// endpoint			:	'',
			// maxAttempts			:	0,
			// region				:	'',
			// requestHandler		:	,
			// retryStrategy		:	,
			// runtime				:	,
			// signer				:	,
			// signingEscapePath	:	,
			// systemClockOffset	:	0,
			// useDualstackEndpoint:	false,
			// useFipsEndpoint		:	false,
		};

		return new LambdaClient(LambdaClientConfig);

	}

	async deploy(){
		//
		console.log(`🚀 Deploying Function: '${this.config.FunctionName}'`);
		console.log(`📖 From source directory: '${this.source_directory}'`);
		console.log(`🙅 Without source patterns: '${this.ignore_patterns}'`);

		//	Update configuration
		await this.deployConfiguration();

		//	Update code
		await this.deployCode();

	}//method

	//	@return	{Promise}	Resolves with the response object when the update is complete.
	async deployCode(){

		//	Zip
		const zip	=	Zip.from(
			this.source_directory,
			this.ignore_patterns,
		);

		console.log('🚀', 'Uploading function code');
		await this._updateFunctionCode({
			FunctionName	:	this.config.FunctionName,
			ZipFile			:	zip.toBuffer(),
		});

		//	Check to make sure the function is updated and can receive another update.
		console.log("\t",'... waiting until AWS Lambda confirms code is deployed','⏱');
		return this._waitUntilFunctionUpdated({
			FunctionName	:	this.config.FunctionName,
		});
	}

	//	@return	{Promise}	Resolves with the response object when the update is complete.
	async deployConfiguration(){
		console.log('🚀', 'Uploading function configuration');
		await this._updateFunctionConfiguration();

		//	Check to make sure the function is updated and can receive another update.
		console.log("\t",'... waiting until AWS Lambda confirms configuration is deployed.','⏱');
		return this._waitUntilFunctionUpdated({
			FunctionName	:	this.config.FunctionName,
		});
	}

	/*
	*	@param	{Object}	params					The Object having the following properties
	*	@param	{String}	params.FunctionName		The file contents as a Buffer
	*	@param	{Buffer}	params.ZipFile			The zipped package contents as a Buffer.
	*	@return	{Promise}							Resolves with the response object when the update is complete.
	*/
	async _updateFunctionCode({
		FunctionName,
		ZipFile,
	}){

		const UpdateFunctionCodeCommandInput = {
			// Architectures	:	['x86_64'],
			// DryRun				:	false,
			FunctionName,
			// ImageUri			:	'',
			// Publish				:	false,
			// RevisionId			:	'',
			// S3Bucket			:	'',
			// S3Key				:	'',
			// S3ObjectVersion		:	'',
			ZipFile,	//	Uint8Array
		};

		const UpdateFunctionCodeCommandOutput = new UpdateFunctionCodeCommand(UpdateFunctionCodeCommandInput);

		return await this.client.send(UpdateFunctionCodeCommandOutput)
			.then(function(response){
				console.log("\t",`... UpdateFunctionCode result: ${response.$metadata.httpStatusCode}`);
				return response;
			})
			.catch(function(error){
				// console.error(error);
				throw new Error(error);
			})
		;

	}//method


	/*
	*	@param	{Object}	params							The Object having the following properties
	*	@param	{String}	params.FunctionName				The file contents as a Buffer
	*	@param	{Object}	params.Environment
	*	@param	{Object}	params.Environment.Variables	A Map of the environment variables as key:value
	*	@return	{Promise}									Resolves with the response object when the update is complete.
	*/
	async _updateFunctionConfiguration(){

		//	Breakpoint
		if( !UpdateFunctionConfigurationCommandInput )	throw new Error(`
You need a './lambda.config.js' file to deploy.

Make this file by putting '"setupLocal": "node-aws-lambda --do=setupLocal"' as a script in your 'package.json'.
Then run 'npm run setupLocal' .
		`.trim());

		//	Normal Execution

		//	We do not allow defining env variables in `lambda.config.js`; we ignore them.
		//	The reason for this is for information security:
		//	- It is possible that someone would want to commit their configuration details to version control while keeping credentials separate.
		//	- Keeping AWS Lambda Environmental Variables in `.env.lambda` lends itself to doing that.
		delete UpdateFunctionConfigurationCommandInput.Environment.Variables;
		if( dot_env_dot_lambda ){
			UpdateFunctionConfigurationCommandInput.Environment.Variables = dot_env_dot_lambda;
		}

		if( UpdateFunctionConfigurationCommandInput.Environment.Variables ){
			console.log("\t","... Updating Lambda's 'process.env' with:")
			console.log(UpdateFunctionConfigurationCommandInput.Environment.Variables);
		}else{
			console.log("\t","... Not making any changes to Lambda's 'process.env'.")
		}

		const UpdateFunctionConfigurationCommandOutput = new UpdateFunctionConfigurationCommand(UpdateFunctionConfigurationCommandInput)

		return await this.client.send(UpdateFunctionConfigurationCommandOutput)
			.then(function(response){
				console.log("\t",`... UpdateFunctionConfiguration result: ${response.$metadata.httpStatusCode}`);
				// console.log(`Current function status: ${response.LastUpdateStatus}`);
				// console.log(response);
				return response;
			})
			.catch(function(error){
				// console.error(error);
				throw new Error(error);
			})
		;

	}//method

	/*
	*	@param	{Object}	params					The Object having the following properties
	*	@param	{String}	params.FunctionName		The file contents as a Buffer
	*	@param	{String}	params.S3Bucket			The name of the AWS S3 Bucket
	*	@param	{String}	params.S3Key			The mime-type of the file
	*	@return	{Promise}							Resolves with the response object when the update is complete.
	*/
	async _waitUntilFunctionUpdated({
		FunctionName,
	}){

		const GetFunctionConfigurationCommandInput = {
			FunctionName,
			// Qualifier	:	'',
		};

		const WaiterConfiguration = {
			// abortController	:	,	//	<AbortController>,
			// abortSignal		:	,	//	<AbortController["signal"]>
			client			:	this.client,
			// maxDelay			:	10,	//	Seconds
			// maxWaitTime		:	60,	//	Seconds
			// minDelay			:	1,	//	Seconds
		};

		return await waitUntilFunctionUpdated(WaiterConfiguration,GetFunctionConfigurationCommandInput)
			.then(function(response){
				console.log("\t",response.state==='SUCCESS'?'✅ ':'🚨 ',`waitUntilFunctionUpdated result: ${response.state}`);
				return response;
			})
			.catch(function(error){
				// console.error(error);
				throw new Error(error);
			})
		;

	}//method

	//	@param	handler {Function}	This is the main Lambda function. It is the handler you export from you Lambda's index.js .
	//	@param	event	{Object}	The event payload being fed into the Lambda function.
	//	@param	context	{Object}	The execition context being fed intot he Lambda function.
	//	@return 		{*}			Proxies the value returned from the handler.
	async execute(testEventFileLocation='test.event.json'){
		const {handler} = await import(`${process.env.INIT_CWD}/index.js`);
		const event = JSON.parse(await FS.readFile(`${process.env.INIT_CWD}/${testEventFileLocation}`));
		const context = JSON.parse(await FS.readFile(`${process.env.INIT_CWD}/test.context.json`));
		console.log('With:');
		console.log("\t",'event',event);
		console.log("\t",'context',context);
		return await handler(event,context);
	}

};

export default Lambda.from();