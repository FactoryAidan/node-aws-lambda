import {listRecursive} from '../Filesystem/index.js';
import AdmZip from 'adm-zip';

export default class {

	static from(
		given_directory	=	'./',
		ignore_patterns	=	['.env','.git'],
	){
		//	Get File List
		const file_list	=	listRecursive(given_directory,ignore_patterns);

		//	Left-Trim
		//	This is the RegularExpression pattern to left-trim the starting filepath from the upload key
		const escaped_given_directory	=	given_directory.replace(/\//g,'\\/').replace(/\./g,'\\.');
		const regex_pattern				=	new RegExp(`^${escaped_given_directory}\/`);

		//	Zip
		console.log('🗜 Compressing files into .zip');
		const zip		=	new AdmZip;
		file_list.forEach(function(pathfilename){
			const left_trimmed_pathfilename	=	pathfilename.replace(regex_pattern,'');
			const path_only					=	left_trimmed_pathfilename.split('/').slice(0,-1).join('/');
			zip.addLocalFile(pathfilename,path_only);
		});

		console.log("\t",'... zipped ✅');

		return zip;		
	}

};
