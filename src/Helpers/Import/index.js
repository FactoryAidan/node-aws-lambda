import path from 'path';

export default async function failsafeImport(module_path){
	try{
		return await import(module_path);
	}catch(error){
		console.error(`
🚨 🚨 🚨 🚨
Unable to import: '${path.basename(module_path)}'
🚨 🚨 🚨 🚨
`);
		return {};
	}
}
