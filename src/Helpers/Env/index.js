import FS from 'fs/promises';

async function get( env_lambda_filename='./.env.lambda' ){
    const file_exists	=	await FS.access(env_lambda_filename)
        .then(result=>true,error=>false);

    //  Breakpoint
    if( !file_exists )	return;

    //  Proceed
    const contents  	=	await FS.readFile(env_lambda_filename);
    const lines		    =	contents.toString('utf8') .split('\n');

    //  Extract Variables
    const env_variables	=	{};
    lines.forEach(function(line){
        if( !line.trim() )	return;	//	If line is empty
        const [key,value]	        =	line.split(/=(.+)/);    //  Split only on first '='.
        env_variables[key.trim()]	=	value.trim();
    });

    //  Return
    return env_variables;
};

export default {
    get,
}