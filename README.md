# What's this?
A package to locally develop & test code meant to run as an AWS Lambda... and then easily deploy that code.

### Unimportant Note
> Although usage can be made easier when a NPM package is installed globally, I personally hate when a project is not self-contained; things are supposed to '_have batteries included_'. Sorry if you think it's easier to bloat your machine with global modules.
As a result of this philosophy, you must manually add some `"scripts"` commands to your existing `package.json` (_outlined below_).

# How do I begin?

Make a AWS Lambda in the AWS web-console. (_It has to exist before this module can upload to it._)

> Create a new project folder, initialize npm into it, & install this package.

```
mkdir your_new_project_folder;

cd your_new_project_folder;

npm init;

npm install https://FactoryAidan@bitbucket.org/FactoryAidan/aws-lambda.git;
```
> Put the following scripts in your project's `package.json`.
```
{
  "type": "module",
  "scripts": {
    "deploy": "node-aws-lambda --do=deploy",
    "deployCode": "node-aws-lambda --do=deployCode",
    "deployConfiguration": "node-aws-lambda --do=deployConfiguration",
    "execute": "node-aws-lambda --do=execute",
    "setupLocal": "node-aws-lambda --do=setupLocal"
  }
}
```
> Initialize an boiler-plate Lambda function
1.  `npm run setupLocal;`
1.  Put your **AWS IAM credentials** & target **AWS Region** into `.env`.
1.  Put your Lambda Function Name into `lambda.config.js`.

# How do I deploy?

Deploy both `code` & `configuraiton`:
```
npm run deploy;
```
Deploy one of them (_to save deployment time_):
```
npm run deployCode;
//  or
npm run deployConfiguration;
```

# How do I test locally?

```
npm run execute;
```
> Your `index.js`'s handler function receives two arguments (_`event` & `context`_).\
> You can control these arguments in your local environment by modifying the contents of `test.context.json` & `test.event.json`.\
> Your `.env` file must be used for Environmental Variables. (_not `.env.lambda`_)

# Environmental Variables

`.env` is what local executions of your Lambda code consume.
`.env.lambda` is what cloud executions of your Lambda code consume; these are deployed to the cloud for you but not used locally.

> There will probably be some duplicated Variables between those two files... but that's how things are to keep things separate & senseful.

# Things you should know

Running `npm run deployConfiguration` is **desctructive**.
> Any definitions found in your `lambda.config.js` will **overwrite** settings stored in the cloud. However, any property not defined in your `lambda.config.js` will remain unchanged in the cloud.
